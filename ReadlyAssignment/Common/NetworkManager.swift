//
//  NetworkManager.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 03.04.2023.
//

import Foundation

class NetworkManager {
    static func call(_ completion: @escaping (Products) -> Void) {
        guard let url = URL(string: "https://dummyjson.com/products") else {
            return
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                return
            }
            do {
                let result = try JSONDecoder().decode(Products.self, from: data)
                completion(result)
            } catch let error {
                print(error)
            }

        }.resume()
    }
}
