//
//  Models.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 03.04.2023.
//

import Foundation
import RealmSwift

// MARK: - JSON Decodable model

struct Products: Decodable {
    var products: [Product]

    subscript(index: Int) -> Product {
        return products[index]
    }

    var count: Int {
        return products.count
    }
}

struct Product: Decodable {
    var id: Int
    var title: String
    var description: String
    var price: Int
    var discountPercentage: Double
    var rating: Double
    var stock: Int
    var brand: String
    var category: String
    var thumbnail: String
    var images: [String]

    init(product: ProductRealm) {
        id = product._id
        title = product.title
        description = product.productDescription
        price = product.price
        discountPercentage = product.discountPercentage
        rating = product.rating
        stock = product.stock
        brand = product.brand
        category = product.category
        thumbnail = product.thumbnail
        images = Array(product.images)
    }
}

// MARK: - Realm model

class ProductRealm: Object {
    @Persisted(primaryKey: true) var _id: Int
    @Persisted var title: String
    @Persisted var productDescription: String
    @Persisted var price: Int
    @Persisted var discountPercentage: Double
    @Persisted var rating: Double
    @Persisted var stock: Int
    @Persisted var brand: String
    @Persisted var category: String
    @Persisted var thumbnail: String
    @Persisted var images: List<String>

    convenience init(product: Product) {
        self.init()

        _id = product.id
        title = product.title
        productDescription = product.description
        price = product.price
        discountPercentage = product.discountPercentage
        rating = product.rating
        stock = product.stock
        brand = product.brand
        category = product.category
        thumbnail = product.thumbnail
        images.append(objectsIn: product.images)
    }
}
