//
//  UICollectionView + Extension.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 04.04.2023.
//

import Foundation
import UIKit

extension UITableView {
    func register<T: UITableViewCell>(_: T.Type) {
        register(T.self, forCellReuseIdentifier: String(describing: T.self))
    }

    public func dequeueReusableCell<T: UITableViewCell>(for type: T.Type, indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as? T else {
            fatalError("Failed to dequeue cell.")
        }

        return cell
    }
}
