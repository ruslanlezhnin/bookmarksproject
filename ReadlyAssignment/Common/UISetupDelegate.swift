//
//  UISetupDelegate.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 04.04.2023.
//

import Foundation

protocol UISetupDelegate {
    func addSubviews()
    func setupConstraints()
}
