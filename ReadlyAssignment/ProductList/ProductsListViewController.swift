//
//  ProductsListViewController.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 03.04.2023.
//

import UIKit
import SnapKit

class ProductsListViewController: UIViewController {

    // MARK: - View Model

    var viewModel: ProductsListControllerViewModel! {
        didSet {
            viewModel.viewModelDidLoadData = { [weak self] in
                DispatchQueue.main.async {
                    self?.productsTableView.reloadData()
                }
            }
        }
    }

    // MARK: - Properties

    private lazy var productsTableView: UITableView = {
        var tableView = UITableView(frame: .zero)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.allowsSelection = true
        
        tableView.register(ProductTableCell.self)

        return tableView
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        viewModel.loadData()

        addSubviews()
        setupConstraints()
    }
}

// MARK: - UISetupDelegate

extension ProductsListViewController: UISetupDelegate {
    func addSubviews() {
        [
            productsTableView
        ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
    }

    func setupConstraints() {
        productsTableView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }
}

// MARK: - UITableViewDataSource

extension ProductsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductTableCell = tableView.dequeueReusableCell(for: ProductTableCell.self, indexPath: indexPath)
        let cellViewModel = viewModel.cellViewModels[indexPath.row]
        cell.configure(viewModel: cellViewModel)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ProductsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = ProductDetailsViewController()
        controller.viewModel = ProductDetailsControllerViewModel(
            product: viewModel.product(index: indexPath.row)
        )
        navigationController?.pushViewController(controller, animated: true)
    }
}
