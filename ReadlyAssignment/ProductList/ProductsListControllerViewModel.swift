//
//  ProductsListViewControllerViewModel.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 04.04.2023.
//

import Foundation
import RealmSwift

class ProductsListControllerViewModel {

    // MARK: - Properties

    var viewModelDidLoadData: (() -> Void)?

    var cellViewModels: [ProductTableCellViewModel] = []

    var numberOfRows: Int {
        return products.count
    }

    private var products: [Product] = []
    private var realm: Realm
    private var isLoadFromNetwork: Bool
    private var realmNotificationToken: NotificationToken?

    // MARK: - Init

    init(loadFromNetwork: Bool) {
        self.isLoadFromNetwork = loadFromNetwork
        realm = try! Realm()

        realmNotificationToken = realm.observe { [weak self] notification, realm in
            if let isLoadFromNetwork = self?.isLoadFromNetwork, !isLoadFromNetwork {
                self?.loadFromRealm()
            }
        }
    }

    // MARK: - Public methods
    
    func loadData() {
        if isLoadFromNetwork {
            loadFromNetwork()
        } else {
            loadFromRealm()
        }
    }

    func product(index: Int) -> Product {
        return products[index]
    }

    // MARK: - Private methods

    private func loadFromNetwork() {
        NetworkManager.call { [weak self] result in
            self?.products = result.products
            result.products.forEach {
                self?.cellViewModels.append(
                    ProductTableCellViewModel(
                        title: $0.title,
                        imageUrl: URL(string: $0.thumbnail)
                    )
                )
            }
            self?.viewModelDidLoadData?()
        }
    }

    private func loadFromRealm() {
        let productsRealm = Array(realm.objects(ProductRealm.self))

        cellViewModels = []
        products = []

        productsRealm.forEach {
            products.append(Product(product: $0))

            cellViewModels.append(
                ProductTableCellViewModel(
                    title: $0.title,
                    imageUrl: URL(string: $0.thumbnail)
                )
            )
        }
        viewModelDidLoadData?()
    }
}
