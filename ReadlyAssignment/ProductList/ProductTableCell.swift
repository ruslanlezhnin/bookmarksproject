//
//  ProductCollectionCell.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 04.04.2023.
//

import UIKit
import SDWebImage

// MARK: - ProductTableCellViewModel

struct ProductTableCellViewModel {
    var title: String
    var imageUrl: URL?
}

// MARK: - ProductTableCell

class ProductTableCell: UITableViewCell {
    private lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()

    private lazy var image: UIImageView = {
        let image = UIImageView()
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.backgroundColor = .lightGray
        return image
    }()

    private lazy var title: UILabel = {
        let label = UILabel()
        return label
    }()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        addSubviews()
        setupConstraints()
    }

    func configure(viewModel: ProductTableCellViewModel) {
        image.sd_setImage(with: viewModel.imageUrl)
        title.text = viewModel.title
    }
}

// MARK: - UISetupDelegate

extension ProductTableCell: UISetupDelegate {
    func addSubviews() {
        containerView.translatesAutoresizingMaskIntoConstraints = false
        [
            title,
            image
        ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            containerView.addSubview($0)
        }
        addSubview(containerView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints { make in
            make.top.bottom.leading.trailing.equalToSuperview()
        }

        image.snp.makeConstraints { make in
            make.height.equalTo(200)
            make.top.equalToSuperview().offset(12)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
        }
        title.snp.makeConstraints { make in
            make.leading.equalTo(image.snp.leading)
            make.trailing.equalTo(image.snp.trailing)
            make.top.equalTo(image.snp.bottom)
            make.bottom.equalToSuperview().offset(-12)
        }
    }
}
