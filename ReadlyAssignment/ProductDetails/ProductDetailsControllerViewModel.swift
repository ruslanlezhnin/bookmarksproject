//
//  ProductDetailsControllerViewModel.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 04.04.2023.
//

import Foundation
import RealmSwift

struct ProductDetailsControllerViewModel {

    // MARK: - Properties

    private var realm: Realm

    private var product: Product

    var didAddBookmark: ((Bool) -> Void)?

    var titleText: String {
        product.title
    }

    var descriptionText: String {
        product.description
    }

    var priceText: String {
        "Price: \(product.price)"
    }

    var discountText: String {
        "Discount: \(product.discountPercentage)"
    }

    var ratingText: String {
        "Rating: \(product.rating)"
    }

    var stockText: String {
        "In stock: \(product.stock)"
    }

    var brandText: String {
        "Brand: \(product.brand)"
    }

    var categoryText: String {
        "Category: \(product.category)"
    }

    var isAlreadyBookmarked: Bool {
        realm.object(ofType: ProductRealm.self, forPrimaryKey: product.id) != nil
    }

    // MARK: - Init

    init(product: Product) {
        self.product = product
        realm = try! Realm()
    }

    // MARK: - Public methods

    func bookmark() {
        if isAlreadyBookmarked {
            removeBookmark()
        } else {
            addBookmark()
        }
    }

    // MARK: - Private methods

    private func addBookmark() {
        do {
            try realm.write {
                realm.add(ProductRealm(product: product))
            }
            didAddBookmark?(true)
        } catch {
            didAddBookmark?(false)
        }
    }

    private func removeBookmark() {
        do {
            try realm.write {
                if let objectToDelete = realm.object(
                    ofType: ProductRealm.self,
                    forPrimaryKey: product.id
                ) {
                    realm.delete(objectToDelete)
                }
            }
            didAddBookmark?(false)
        } catch {
            didAddBookmark?(true)
        }
    }
}

// MARK: - ImageViewerDelegate

extension ProductDetailsControllerViewModel: ImageViewerDelegate {
    func numberOfImages() -> Int {
        return product.images.count
    }

    func imageUrlForIndex(index: Int) -> URL? {
        return URL(string: product.images[index])
    }
}
