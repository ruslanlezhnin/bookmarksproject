//
//  ProductDetailsViewController.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 04.04.2023.
//

import UIKit

class ProductDetailsViewController: UIViewController {

    // MARK: - View Model

    var viewModel: ProductDetailsControllerViewModel! {
        didSet {
            viewModel.didAddBookmark = { [weak self] result in
                if result {
                    self?.bookmarkButton.setTitle("Remove from bookmarks", for: .normal)
                } else {
                    self?.bookmarkButton.setTitle("Bookmark", for: .normal)
                }
            }
        }
    }

    // MARK: - Properties

    private lazy var imageViewer: ImageViewerView = {
        let viewer = ImageViewerView()
        viewer.delegate = viewModel
        return viewer
    }()

    lazy var bookmarkButton: UIButton = {
        let button = UIButton(configuration: .borderedProminent())
        let title = viewModel.isAlreadyBookmarked
        ? "Remove from bookmarks"
        : "Bookmark"
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: #selector(bookmarkAction), for: .touchUpInside)
        return button
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(24)
        label.text = viewModel.titleText
        label.numberOfLines = 0
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(16)
        label.text = viewModel.descriptionText
        label.numberOfLines = 0
        return label
    }()

    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.text = viewModel.priceText
        return label
    }()

    private lazy var discountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.text = viewModel.discountText
        return label
    }()

    private lazy var ratingLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.text = viewModel.ratingText
        return label
    }()

    private lazy var stockLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.text = viewModel.stockText
        return label
    }()

    private lazy var brandLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.text = viewModel.brandText
        return label
    }()

    private lazy var categoryLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.text = viewModel.categoryText
        return label
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSubviews()
        setupConstraints()
    }

    // MARK: - Button Actions

    @objc private func bookmarkAction() {
        viewModel.bookmark()
    }
}

// MARK: - UISetupDelegate

extension ProductDetailsViewController: UISetupDelegate {
    func addSubviews() {
        [
            imageViewer,
            bookmarkButton,
            titleLabel,
            descriptionLabel,
            priceLabel,
            discountLabel,
            ratingLabel,
            stockLabel,
            brandLabel,
            categoryLabel
        ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }
    }

    func setupConstraints() {
        imageViewer.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(300)
        }

        bookmarkButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide).offset(-20)
            make.height.equalTo(40)
            make.width.equalTo(200)
            make.centerX.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalTo(imageViewer.snp.bottom).offset(20)
        }

        descriptionLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
        }

        priceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalTo(descriptionLabel.snp.bottom).offset(10)
        }

        discountLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalTo(priceLabel.snp.bottom).offset(10)
        }

        ratingLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalTo(discountLabel.snp.bottom).offset(10)
        }

        stockLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalTo(ratingLabel.snp.bottom).offset(10)
        }

        brandLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalTo(stockLabel.snp.bottom).offset(10)
        }

        categoryLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalTo(brandLabel.snp.bottom).offset(10)
        }
    }
}
