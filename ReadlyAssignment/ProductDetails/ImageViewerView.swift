//
//  ImageViewerView.swift
//  ReadlyAssignment
//
//  Created by Ruslan on 04.04.2023.
//

import UIKit
import SDWebImage

// MARK: - ImageViewerDelegate

protocol ImageViewerDelegate {
    func numberOfImages() -> Int
    func imageUrlForIndex(index: Int) -> URL?
}

// MARK: - ImageViewerView

class ImageViewerView: UIView {

    // MARK: - Properties

    var delegate: ImageViewerDelegate? {
        didSet {
            setImage()
        }
    }

    private var currentIndex = 0

    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.backgroundColor = .lightGray
        return image
    }()

    lazy var leftButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        return button
    }()

    lazy var rightButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        return button
    }()

    lazy var imageCountLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Button Actions

    @objc private func leftButtonAction() {
        guard let delegate, delegate.numberOfImages() > 0 else {
            return
        }

        if currentIndex == 0 {
            currentIndex = delegate.numberOfImages() - 1
        } else {
            currentIndex -= 1
        }

        setImage()
    }

    @objc private func rightButtonAction() {
        guard let delegate, delegate.numberOfImages() > 0 else {
            return
        }

        if currentIndex == delegate.numberOfImages() - 1 {
            currentIndex = 0
        } else {
            currentIndex += 1
        }

        setImage()
    }

    // MARK: - Private methods

    private func setImage() {
        guard
            let delegate,
            let url = delegate.imageUrlForIndex(index: currentIndex) else {
            return
        }

        imageView.sd_setImage(with: url)
    }
}

// MARK: - UISetupDelegate

extension ImageViewerView: UISetupDelegate {
    func addSubviews() {
        [
            imageView,
            leftButton,
            rightButton
        ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }
    }

    func setupConstraints() {
        imageView.snp.makeConstraints { make in
            make.top.bottom.leading.trailing.equalToSuperview()
        }

        leftButton.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
        }

        rightButton.snp.makeConstraints { make in
            make.trailing.top.bottom.equalToSuperview()
            make.leading.equalTo(leftButton.snp.trailing)
            make.width.equalTo(leftButton.snp.width
            ).multipliedBy(1.0 / 1.0)
        }
    }
}
